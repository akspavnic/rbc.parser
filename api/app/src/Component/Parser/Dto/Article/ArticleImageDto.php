<?php

declare(strict_types=1);

namespace App\Component\Parser\Dto\Article;

class ArticleImageDto
{
    public function __construct(
        private string $url,
        private ?string $author
    ) {}

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }
}
