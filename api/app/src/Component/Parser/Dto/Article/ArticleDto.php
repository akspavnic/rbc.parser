<?php

declare(strict_types=1);

namespace App\Component\Parser\Dto\Article;

class ArticleDto
{
    /**
     * @param ArticleAuthorDto[] $authors
     * @param ArticleImageDto[] $images
     * @param ArticleTagDto[] $tags
     */
    public function __construct(
        private string              $title,
        private string              $lead,
        private string              $content,
        private array               $authors = [],
        private array               $images = [],
        private array               $tags = [],
        private ?string             $externalId = null,
        private ?\DateTimeInterface $datetimePublished = null,
    ) {}

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDatetimePublished(): ?\DateTimeInterface
    {
        return $this->datetimePublished;
    }

    public function getLead(): string
    {
        return $this->lead;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return ArticleAuthorDto[]
     */
    public function getAuthors(): array
    {
        return $this->authors;
    }

    /**
     * @return ArticleImageDto[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @return ArticleTagDto[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }
}
