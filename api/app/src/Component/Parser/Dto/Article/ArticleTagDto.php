<?php

declare(strict_types=1);

namespace App\Component\Parser\Dto\Article;

class ArticleTagDto
{
    public function __construct(
        private string $tag
    ) {}

    public function getTag(): string
    {
        return $this->tag;
    }
}
