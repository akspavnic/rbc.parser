<?php

declare(strict_types=1);

namespace App\Component\Parser\Dto\Article;

class ArticleAuthorDto
{
    public function __construct(
        private string $name,
    ) {}

    public function getName(): string
    {
        return $this->name;
    }
}
