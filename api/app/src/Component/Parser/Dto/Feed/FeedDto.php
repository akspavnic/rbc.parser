<?php

declare(strict_types=1);

namespace App\Component\Parser\Dto\Feed;

class FeedDto
{
    /**
     * @param FeedItemDto[] $articles
     */
    public function __construct(
        private array $articles = [],
    ) {}

    /**
     * @return FeedItemDto[]
     */
    public function getItems(): array
    {
        return $this->articles;
    }
}
