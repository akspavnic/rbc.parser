<?php

declare(strict_types=1);

namespace App\Component\Parser\Dto\Feed;

class FeedItemDto
{
    public function __construct(
        private ?string $externalId,
        private ?string $title,
        private ?string $url,
        private ?\DateTimeInterface $datetimePublished
    ) {}

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getDatetimePublished(): ?\DateTimeInterface
    {
        return $this->datetimePublished;
    }
}
