<?php

declare(strict_types=1);

namespace App\Component\Parser\Builder\Article;

use App\Component\Parser\Dto\Article\ArticleDto;

interface ArticleDetailBuilderInterface
{
    public function build(string $body): ArticleDto;
}