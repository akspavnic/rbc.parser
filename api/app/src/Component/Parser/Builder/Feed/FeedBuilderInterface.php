<?php

declare(strict_types=1);

namespace App\Component\Parser\Builder\Feed;

use App\Component\Parser\Dto\Feed\FeedDto;

interface FeedBuilderInterface
{
    public function build(string $body): FeedDto;
}