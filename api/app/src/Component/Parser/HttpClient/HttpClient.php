<?php

declare(strict_types=1);

namespace App\Component\Parser\HttpClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class HttpClient implements HttpClientInterface
{
    private const HTTP_CLIENT_DEFAULT_REQUEST_OPTIONS = [
        RequestOptions::VERIFY => false,
    ];

    /**
     * @throws GuzzleException
     */
    public function getData(HttpRequestInterface $request): string
    {
        $client   = new Client(self::HTTP_CLIENT_DEFAULT_REQUEST_OPTIONS);
        $response = $client->request(
            $request->getMethod(),
            $request->getUrl(),
            $request->getOptions()
        );

        return $response->getBody()->getContents();
    }
}