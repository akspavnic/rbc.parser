<?php

declare(strict_types=1);

namespace App\Component\Parser\HttpClient;

use App\Component\Parser\Dto\Http\HttpRequestDto;

interface HttpClientInterface
{
    public function getData(HttpRequestInterface $request): ?string;
}