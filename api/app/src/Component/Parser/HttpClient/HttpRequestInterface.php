<?php

declare(strict_types=1);

namespace App\Component\Parser\HttpClient;

interface HttpRequestInterface
{
    public function getUrl(): string;
    public function getMethod(): string;
    public function getOptions(): array;
}