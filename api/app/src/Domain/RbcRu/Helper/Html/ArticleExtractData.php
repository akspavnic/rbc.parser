<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Helper\Html;

use App\Component\Parser\Dto\Article\ArticleAuthorDto;
use App\Component\Parser\Dto\Article\ArticleImageDto;
use App\Component\Parser\Dto\Article\ArticleTagDto;
use Symfony\Component\DomCrawler\Crawler;

class ArticleExtractData
{
    public static function getIdFromUrl(string $url): ?string
    {
        $partials = explode('/', $url);
        return !empty($partials) ? end($partials) : null;
    }

    public static function getTitle(Crawler $crawler): string
    {
        return $crawler->filter('h1')->text();
    }

    /**
     * @return ArticleImageDto[]
     */
    public static function getImages(Crawler $crawler): array
    {
        $imagesSelectors = [
            [
                'items' => '.article__text .article__main-image',
                'author' => '.article__main-image__author'
            ],
            [
                'items' => '.article__text .article__picture',
                'author' => '.article__picture__author'
            ],
        ];

        $images = [];

        foreach ($imagesSelectors as $selector) {
            $images[] = array_filter(
                $crawler->filter($selector['items'])->each(static function(Crawler $crawler) use ($selector) {
                    $url = $crawler->filter('img')->attr('src');
                    $author = $crawler->filter($selector['author'])->text();

                    if (null !== $url && null !== $author) {
                        return new ArticleImageDto($url, $author);
                    }

                    return null;
                })
            );
        }

        return array_merge(...$images);
    }

    public static function getLead(Crawler $crawler): ?string
    {
        return $crawler->filter('.article__text .article__text__overview')->text();
    }

    /**
     * @return ArticleAuthorDto[]
     */
    public static function getAuthors(Crawler $crawler): array
    {
        return array_filter(
            $crawler->filter('.article__authors .article__authors__row')
                ->each(static function(Crawler $crawler) {
                    $name      = $crawler->text();

                    if (null !== $name) {
                        return new ArticleAuthorDto($name);
                    }

                    return null;
                }
            )
        );
    }

    /**
     * @return ArticleTagDto[]
     */
    public static function getTags(Crawler $crawler): array
    {
        return array_filter(
            $crawler->filter('.article__tags .article__tags__item')
            ->each(static function (Crawler $crawler) {

                $tag = $crawler->text();
                if (null !== $tag) {
                    return new ArticleTagDto($tag);
                }

                return null;
            })
        );
    }

    public static function getContent(Crawler $crawler): ?string
    {
        $excludeClassesFilter = [
            'article__inline-item',
            'banner',
        ];

        $findDetailTextFilter = [
            '.article__text > .article__text__main',
            '.article__text',
        ];

        foreach ($findDetailTextFilter as $selector) {
            try {
                $chlidren = $crawler->filter($selector)->children();
            } catch (\Exception) {
                continue;
            }

            $nodes = array_filter(
                $chlidren
                    ->each(static function(Crawler $crawler) use ($excludeClassesFilter) {

                        if (in_array($crawler->attr('class'), $excludeClassesFilter, true)) {
                            return null;
                        }

                        return $crawler->outerHtml();
                    })
            );

            return implode("\r\n", $nodes);
        }

        return null;
    }
}
