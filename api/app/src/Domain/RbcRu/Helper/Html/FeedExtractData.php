<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Helper\Html;

use Symfony\Component\DomCrawler\Crawler;

class FeedExtractData
{
    public static function getFeedItems(string $html): array
    {
        $crawler   = new Crawler($html);
        return $crawler->filter('a.news-feed__item')->each(static function (Crawler $node, $i) {
            return static::parseNode($node);
        });
    }

    public static function getFeedItem(string $html): array
    {
        return static::getFeedItems($html)[0];
    }

    public static function isValidValues(array $data): bool
    {
        foreach ($data as $value) {
            if (empty($value)) {
                return false;
            }
        }

        return true;
    }

    private static function parseNode(Crawler $node): array
    {
        return [
            'id'        => str_replace('id_newsfeed_', '', $node->attr('id') ?? ''),
            'url'       => $node->attr('href'),
            'title'     => $node->filter('.news-feed__item__title')->text(),
            'timestamp' => $node->attr('data-modif'),
        ];
    }
}