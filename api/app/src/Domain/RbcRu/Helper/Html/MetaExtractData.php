<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Helper\Html;

use App\Domain\RbcRu\Dto\Article\MetaDto;
use App\Domain\RbcRu\Dto\Article\MetaTagDto;
use Symfony\Component\DomCrawler\Crawler;

class MetaExtractData
{
    public static function getMeta(Crawler $crawler): MetaDto
    {
        $tags = array_filter(
            $crawler->filter('meta')->each(static function(Crawler $crawler) {
                foreach (['property', 'itemprop'] as $propName) {
                    if (
                        (null !== $prop = $crawler->attr($propName))
                        && (null !== $content = $crawler->attr('content'))
                    ) {
                        return new MetaTagDto(
                            $prop,
                            $content,
                        );
                    }
                }

                return null;
            })
        );

        return new MetaDto($tags);
    }
}