<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Service\Db;

use App\Component\Parser\Dto\Article\ArticleDto;
use App\Component\Parser\Dto\Feed\FeedDto;
use App\Domain\RbcRu\Entity\Article;
use App\Domain\RbcRu\Entity\FeedItem;
use App\Domain\RbcRu\Exception\Feed\NotFoundFeedItemException;
use App\Domain\RbcRu\Repository\ArticleRepository;
use App\Domain\RbcRu\Repository\FeedItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class DbService
{
    public function __construct(
        private EntityManagerInterface $em,
    ) {}

    public function getEm(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @return EntityRepository|FeedItemRepository
     */
    public function getFeedItemRepository(): EntityRepository
    {
        return $this->em->getRepository(FeedItem::class);
    }

    /**
     * @return EntityRepository|ArticleRepository
     */
    public function getArticleRepository(): EntityRepository
    {
        return $this->em->getRepository(Article::class);
    }

    public function upsertFeed(FeedDto $feedDto): void
    {
        foreach ($feedDto->getItems() as $item) {
            $feedItem = $this->getFeedItemRepository()->findOneBy([
                'externalId' => $item->getExternalId(),
            ]);

            if (null === $feedItem) {
                $feedItem = new FeedItem();
                $this->em->persist($feedItem);
            }

            $feedItem->setExternalId($item->getExternalId());
            $feedItem->setDatetimePublished($item->getDatetimePublished());
            $feedItem->setTitle($item->getTitle());
            $feedItem->setUrl($item->getUrl());
        }

        $this->em->flush();
    }

    public function upsertArticle(ArticleDto $articleDto): void
    {
        $feedItem = $this->getFeedItemRepository()->findOneBy([
            'externalId' => $articleDto->getExternalId(),
        ]);

        if (null === $feedItem) {
            throw new NotFoundFeedItemException();
        }

        $article = $this->getArticleRepository()->findOneBy([
            'externalId' => $articleDto->getExternalId(),
        ]);

        if (null === $article) {
            $article = new Article();
            $this->em->persist($article);
        }

        $article->setFeedItem($feedItem);

        $article->setTitle($articleDto->getTitle());
        $article->setLead($articleDto->getLead());
        $article->setContent($articleDto->getContent());
        $article->setAuthors(array_map(
            static fn($author) => $author->getName(),
            $articleDto->getAuthors()
        ));
        $article->setImages(array_map(
            static fn($img) => $img->getUrl(),
            $articleDto->getImages()
        ));
        $article->setTags(array_map(
            static fn($tag) => $tag->getTag(),
            $articleDto->getTags()
        ));
        $article->setExternalId($articleDto->getExternalId());
        $article->setDatetimePublished($articleDto->getDatetimePublished());

        $this->em->flush();
    }
}