<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Service\Parser;

use App\Component\Parser\Dto\Article\ArticleDto;
use App\Component\Parser\HttpClient\HttpRequestInterface;

interface ArticleServiceInterface
{
    public function getArticle(HttpRequestInterface $request): ArticleDto;
}