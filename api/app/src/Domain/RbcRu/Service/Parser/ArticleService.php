<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Service\Parser;

use App\Component\Parser\Dto\Article\ArticleDto;
use App\Component\Parser\HttpClient\HttpClient;
use App\Component\Parser\HttpClient\HttpRequestInterface;
use App\Domain\RbcRu\Builder\Article\ArticleFromHtmlBuilder;

class ArticleService implements ArticleServiceInterface
{
    public function __construct(
        private HttpClient $httpClient,
        private ArticleFromHtmlBuilder $htmlBuilder,
    ) {}

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \App\Domain\RbcRu\Exception\Feed\EmptyContentException
     */
    public function getArticle(HttpRequestInterface $request): ArticleDto
    {
        $body = $this->httpClient->getData($request);

        return $this->htmlBuilder->build($body);
    }
}