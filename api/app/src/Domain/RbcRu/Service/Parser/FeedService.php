<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Service\Parser;

use App\Component\Parser\Builder\Feed\FeedBuilderInterface;
use App\Component\Parser\Dto\Feed\FeedDto;
use App\Component\Parser\HttpClient\HttpClient;
use App\Component\Parser\HttpClient\HttpRequestInterface;
use App\Domain\RbcRu\Builder\Feed\BuilderProvider;
use App\Domain\RbcRu\Builder\Feed\NewsFeedFromHtmlBuilder;
use App\Domain\RbcRu\Builder\Feed\NewsFeedFromJsonBuilder;
use App\Domain\RbcRu\Exception\Feed\BuilderClassUndefinedException;
use App\Domain\RbcRu\Exception\Feed\BuilderNotFoundException;
use App\Domain\RbcRu\Request\Feed\NewsFeedIndexPageRequest;
use App\Domain\RbcRu\Request\Feed\NewsFeedJsonEndpointRequest;

class FeedService implements FeedServiceInterface
{
    private const REQUEST_TO_BUILDER = [
        NewsFeedIndexPageRequest::class     => NewsFeedFromHtmlBuilder::class,
        NewsFeedJsonEndpointRequest::class  => NewsFeedFromJsonBuilder::class,
    ];

    public function __construct(
        private HttpClient $httpClient,
        private BuilderProvider $builderProvider,
    ) {}

    public function getFeed(HttpRequestInterface $request): FeedDto
    {
        $builderClass = self::REQUEST_TO_BUILDER[$request::class] ?? null;

        if (null === $builderClass) {
            throw new BuilderClassUndefinedException();
        }

        /** @var FeedBuilderInterface $builder */
        $builder = $this->builderProvider->getBuilderByClassname($builderClass);

        if (null === $builder) {
            throw new BuilderNotFoundException();
        }

        $body = $this->httpClient->getData($request);

        return $builder->build($body);
    }
}