<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Service\Parser;

use App\Component\Parser\Dto\Feed\FeedDto;
use App\Component\Parser\HttpClient\HttpRequestInterface;

interface FeedServiceInterface
{
    public function getFeed(HttpRequestInterface $request): FeedDto;
}