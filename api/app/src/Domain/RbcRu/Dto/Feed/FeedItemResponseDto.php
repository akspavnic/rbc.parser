<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Dto\Feed;

use Symfony\Component\Validator\Constraints as Assert;

class FeedItemResponseDto
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Type('int')]
        private int $publishDateT,

        #[Assert\NotBlank]
        #[Assert\Type('string')]
        private string $html
    ) {}

    public function getPublishDateT(): int
    {
        return $this->publishDateT;
    }

    public function getDatetime(): \DateTimeInterface
    {
        return (new \DateTime())->setTimestamp($this->getPublishDateT());
    }

    public function getHtml(): string
    {
        return $this->html;
    }
}