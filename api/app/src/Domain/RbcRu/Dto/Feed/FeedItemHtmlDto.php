<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Dto\Feed;

use Symfony\Component\Validator\Constraints as Assert;

class FeedItemHtmlDto
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Type('string')]
        private string $id,

        #[Assert\NotBlank]
        #[Assert\Url]
        private string $url,

        #[Assert\NotBlank]
        #[Assert\Type('string')]
        private string $title,

        #[Assert\NotBlank]
        #[Assert\Type('int')]
        private int $timestamp,
    ) {}

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getDatetime(): \DateTimeInterface
    {
        return (new \DateTime())->setTimestamp($this->getTimestamp());
    }
}
