<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Dto\Article;

class MetaDto
{
    private array $tagsToArray;

    /**
     * @param MetaTagDto[] $tags
     * @param array $tagsToArray
     */
    public function __construct(
        private array $tags,
    ) {
        foreach ($this->tags as $tag) {
            $this->tagsToArray[$tag->getProperty()] = $tag->getContent();
        }
    }

    /**
     * @return MetaTagDto[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    public function toArray(): array
    {
        return $this->tagsToArray;
    }

    public function getValueByProperty(string $property): ?string
    {
        return $this->tagsToArray[$property] ?? null;
    }
}
