<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Dto\Article;

class MetaTagDto
{
    public function __construct(
        private string $property,
        private string $content,
    ) {}

    public function getProperty(): string
    {
        return $this->property;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
