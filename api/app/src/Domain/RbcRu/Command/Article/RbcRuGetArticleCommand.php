<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Command\Article;

use App\Domain\RbcRu\Request\Article\ArticleDetailPageRequest;
use App\Domain\RbcRu\Request\Feed\NewsFeedIndexPageRequest;
use App\Domain\RbcRu\Service\Parser\ArticleService;
use App\Domain\RbcRu\Service\Parser\FeedService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:rbc-ru:article:get',
    description: 'Article parsing',
)]
class RbcRuGetArticleCommand extends Command
{
    public function __construct(
        private ArticleService $articleService,
    )
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument('url', InputArgument::REQUIRED, 'Article detail URL')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $articleUrl = $input->getArgument('url');
        if (empty($articleUrl)) {
            throw new \ErrorException('Url is required');
        }

        $articleDto = $this->articleService->getArticle(new ArticleDetailPageRequest($articleUrl));

        dump($articleDto);

        return Command::SUCCESS;
    }
}
