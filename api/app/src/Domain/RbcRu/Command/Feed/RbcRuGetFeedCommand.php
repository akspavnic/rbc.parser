<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Command\Feed;

use App\Domain\RbcRu\Request\Feed\NewsFeedIndexPageRequest;
use App\Domain\RbcRu\Service\Parser\FeedService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:rbc-ru:feed:get',
    description: 'Feed parsing',
)]
class RbcRuGetFeedCommand extends Command
{
    public function __construct(
        private FeedService $feedService,
    )
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $feed = $this->feedService->getFeed(new NewsFeedIndexPageRequest());

        $rows = [];

        foreach ($feed->getItems() as $item) {
            $rows[] = [
                'externalId' => $item->getExternalId(),
                'title'      => $item->getTitle(),
                'url'        => $item->getUrl(),
                'datetime'   => $item->getDatetimePublished()?->format(DATE_ATOM),
            ];
        }

        $table = new Table($output);
        $table->setHeaders(array_keys($rows[0]));
        $table->setRows($rows);

        $table->render();

        return Command::SUCCESS;
    }
}
