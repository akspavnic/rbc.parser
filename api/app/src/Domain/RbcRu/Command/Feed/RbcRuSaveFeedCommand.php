<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Command\Feed;

use App\Domain\RbcRu\Message\ArticleDetailMessage;
use App\Domain\RbcRu\Request\Feed\NewsFeedIndexPageRequest;
use App\Domain\RbcRu\Service\Db\DbService;
use App\Domain\RbcRu\Service\Parser\FeedService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'app:rbc-ru:feed:save',
    description: 'Feed parsing and save',
)]
class RbcRuSaveFeedCommand extends Command
{
    public function __construct(
        private FeedService $feedService,
        private DbService $dbService,
        private MessageBusInterface $messageBus,
    )
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->addOption(
            'save-articles',
            null,
            InputOption::VALUE_OPTIONAL,
            'Save articles from feed',
            0
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $isSaveArticles = (bool)$input->getOption('save-articles');

        $feed = $this->feedService->getFeed(new NewsFeedIndexPageRequest());

        $this->dbService->upsertFeed($feed);

        if ($isSaveArticles) {
            foreach ($feed->getItems() as $item) {
                $this->messageBus->dispatch(new ArticleDetailMessage($item->getExternalId()));
            }
        }

        $io->success('Success');

        return Command::SUCCESS;
    }
}
