<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Message\Handler;

use App\Domain\RbcRu\Message\ArticleDetailMessage;
use App\Domain\RbcRu\Request\Article\ArticleDetailPageRequest;
use App\Domain\RbcRu\Service\Db\DbService;
use App\Domain\RbcRu\Service\Parser\ArticleService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class ArticleDetailHandler
{
    public function __construct(
        private DbService $dbService,
        private ArticleService $articleService,
        private LoggerInterface $logger,
    )
    {
    }

    public function __invoke(ArticleDetailMessage $message): void
    {
        $feedItem = $this->dbService->getFeedItemRepository()->findOneBy([
            'externalId' => $message->getExternalId(),
        ]);

        if (null === $feedItem) {
            $this->logger->error(__METHOD__, [
                'line' => __LINE__,
                'message' => sprintf('Feed item [#%s] not found in DB', $message->getExternalId()),
            ]);

            return;
        }

        try {
            $articleDto = $this->articleService->getArticle(new ArticleDetailPageRequest($feedItem->getUrl()));
        } catch (\Exception $e) {
            $this->logger->error(__METHOD__, [
                'line' => __LINE__,
                'externalId' => $message->getExternalId(),
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);

            return;
        }

        try {
            $this->dbService->upsertArticle($articleDto);
        } catch (\Exception $e) {
            $this->logger->error(__METHOD__, [
                'line' => __LINE__,
                'externalId' => $message->getExternalId(),
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);

            return;
        }
    }
}