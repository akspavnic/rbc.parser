<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Message;

class ArticleDetailMessage
{
    public function __construct(
        private string $externalId
    ) {}

    public function getExternalId(): string
    {
        return $this->externalId;
    }
}