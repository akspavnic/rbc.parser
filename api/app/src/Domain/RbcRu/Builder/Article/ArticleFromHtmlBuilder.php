<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Builder\Article;

use App\Component\Parser\Builder\Article\ArticleDetailBuilderInterface;
use App\Component\Parser\Dto\Article\ArticleDto;
use App\Domain\RbcRu\Exception\Feed\EmptyContentException;
use App\Domain\RbcRu\Exception\Feed\UnparseableHtmlException;
use App\Domain\RbcRu\Helper\Html\ArticleExtractData;
use App\Domain\RbcRu\Helper\Html\MetaExtractData;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;

class ArticleFromHtmlBuilder implements ArticleDetailBuilderInterface
{
    public function __construct(
        private LoggerInterface $logger,
    ) {}

    public function build(string $body): ArticleDto
    {
        $crawler = new Crawler($body);

        $meta = MetaExtractData::getMeta($crawler);
        $datePublished = $meta->getValueByProperty('datePublished');
        $datePublished = null !== $datePublished ? new \DateTime($datePublished) : null;

        $url = $meta->getValueByProperty('og:url');
        if (null === $url) {
            $this->logger->error(__METHOD__, [
                'line'      => __LINE__,
                'message'   => 'Not found url from og:url',
            ]);

            throw new UnparseableHtmlException('Not found url from og:url');
        }

        $externalId = ArticleExtractData::getIdFromUrl($url);
        $title      = ArticleExtractData::getTitle($crawler);
        $images     = ArticleExtractData::getImages($crawler);
        $lead       = $meta->getValueByProperty('description') ?? ArticleExtractData::getLead($crawler);
        $authors    = ArticleExtractData::getAuthors($crawler);
        $tags       = ArticleExtractData::getTags($crawler);
        $content    = ArticleExtractData::getContent($crawler);

        if (null === $content) {
            throw new EmptyContentException();
        }

        return new ArticleDto(
            $title,
            $lead,
            $content,
            $authors,
            $images,
            $tags,
            $externalId,
            $datePublished,
        );
    }
}
