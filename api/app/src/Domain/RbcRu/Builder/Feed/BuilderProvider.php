<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Builder\Feed;

use App\Component\Parser\Builder\Feed\FeedBuilderInterface;

class BuilderProvider
{
    /**
     * @var FeedBuilderInterface[]
     */
    private array $builders;

    public function __construct(
        private NewsFeedFromHtmlBuilder $feedFromHtmlBuilder,
        private NewsFeedFromJsonBuilder $feedFromJsonBuilder,
    )
    {
        $this->builders = [
            NewsFeedFromHtmlBuilder::class => $this->feedFromHtmlBuilder,
            NewsFeedFromJsonBuilder::class => $this->feedFromJsonBuilder,
        ];
    }

    public function getBuilderByClassname(string $classname): ?FeedBuilderInterface
    {
        return $this->builders[$classname] ?? null;
    }
}