<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Builder\Feed;

use App\Component\Parser\Builder\Feed\FeedBuilderInterface;
use App\Component\Parser\Dto\Feed\FeedItemDto;
use App\Component\Parser\Dto\Feed\FeedDto;
use App\Domain\RbcRu\Dto\Feed\FeedItemHtmlDto;
use App\Domain\RbcRu\Dto\Feed\FeedItemResponseDto;
use App\Domain\RbcRu\Exception\Feed\InvalidFeedFormatException;
use App\Domain\RbcRu\Exception\Feed\UnparseableHtmlException;
use App\Domain\RbcRu\Helper\Html\FeedExtractData;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NewsFeedFromJsonBuilder implements FeedBuilderInterface
{
    private ObjectNormalizer $normalizer;

    public function __construct(
        private ValidatorInterface $validator,
        private LoggerInterface $logger,
    )
    {
        $this->normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter());
    }

    public function build(string $body): FeedDto
    {
        $json = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        $this->logger->info(__METHOD__, [
            'line' => __LINE__,
            'json' => $json,
        ]);

        if (!isset($json['inserted'])) {
            throw new InvalidFeedFormatException();
        }

        $articles = [];

        foreach (array_merge($json['inserted'], $json['updated']) as $item) {
            try {
                /** @var FeedItemResponseDto $feedItemResponseDto */
                $feedItemResponseDto = $this->normalizer->denormalize(
                    $item,
                    FeedItemResponseDto::class,
                    null,
                    [
                        DenormalizerInterface::COLLECT_DENORMALIZATION_ERRORS => true,
                    ]
                );
            } catch (\Exception $e) {
                $this->logger->error(__METHOD__, [
                    'line'      => __LINE__,
                    'exception' => $e::class,
                    'context'   => 'denormalize',
                    'message'   => $e->getMessage(),
                ]);

                continue;
            }

            $errors = $this->validator->validate($feedItemResponseDto);
            if (count($errors) > 0) {
                $this->logger->error(__METHOD__, [
                    'line'    => __LINE__,
                    'context' => 'validate',
                    'message' => (string) $errors,
                ]);

                continue;
            }

            try {
                $htmlDto = $this->parseHtml($feedItemResponseDto->getHtml());
            } catch (\Exception $e) {
                $this->logger->error(__METHOD__, [
                    'line'      => __LINE__,
                    'exception' => $e::class,
                    'context'   => 'parseHtml',
                    'message'   => $e->getMessage(),
                ]);

                continue;
            }

            $article = new FeedItemDto(
                $htmlDto->getId(),
                $htmlDto->getTitle(),
                $htmlDto->getUrl(),
                $htmlDto->getDatetime(),
            );

            $articles[$htmlDto->getId()] = $article;
        }

        return new FeedDto($articles);
    }

    private function parseHtml(string $html): FeedItemHtmlDto
    {
        try {
            $item = FeedExtractData::getFeedItem($html);
        } catch (\Exception $e) {
            throw new UnparseableHtmlException($e->getMessage(), $e->getCode(), $e);
        }

        if (!FeedExtractData::isValidValues($item)) {
            throw new UnparseableHtmlException('Invalid values in news item');
        }

        /** @var FeedItemHtmlDto $htmlDto */
        $htmlDto = $this->normalizer->denormalize(
            $item,
            FeedItemHtmlDto::class,
            null,
            [
                DenormalizerInterface::COLLECT_DENORMALIZATION_ERRORS => true,
            ]
        );

        $errors = $this->validator->validate($htmlDto);
        if (count($errors) > 0) {
            throw new UnparseableHtmlException((string) $errors);
        }

        return $htmlDto;
    }
}