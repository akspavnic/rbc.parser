<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Builder\Feed;

use App\Component\Parser\Builder\Feed\FeedBuilderInterface;
use App\Component\Parser\Dto\Feed\FeedItemDto;
use App\Component\Parser\Dto\Feed\FeedDto;
use App\Domain\RbcRu\Dto\Feed\FeedItemHtmlDto;
use App\Domain\RbcRu\Exception\Feed\EmptyFeedException;
use App\Domain\RbcRu\Helper\Html\FeedExtractData;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NewsFeedFromHtmlBuilder implements FeedBuilderInterface
{
    private ObjectNormalizer $normalizer;

    public function __construct(
        private ValidatorInterface $validator,
        private LoggerInterface $logger,
    )
    {
        $this->normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter());
    }

    public function build(string $body): FeedDto
    {
        $newsItems = FeedExtractData::getFeedItems($body);

        $newsItems = array_filter($newsItems, static function(array $item) {
            return FeedExtractData::isValidValues($item);
        });

        if (empty($newsItems)) {
            $this->logger->error(__METHOD__, [
                'line'      => __LINE__,
                'message'   => 'News feed is empty',
            ]);

            throw new EmptyFeedException();
        }

        $articles = [];

        foreach ($newsItems as $item) {
            try {
                /** @var FeedItemHtmlDto $htmlDto */
                $htmlDto = $this->normalizer->denormalize(
                    $item,
                    FeedItemHtmlDto::class,
                    null,
                    [
                        DenormalizerInterface::COLLECT_DENORMALIZATION_ERRORS => true,
                    ]
                );
            } catch (\Exception $e) {
                $this->logger->error(__METHOD__, [
                    'line'      => __LINE__,
                    'exception' => $e::class,
                    'context'   => 'denormalize',
                    'message'   => $e->getMessage(),
                ]);

                continue;
            }

            $errors = $this->validator->validate($htmlDto);
            if (count($errors) > 0) {
                $this->logger->error(__METHOD__, [
                    'line'    => __LINE__,
                    'context' => 'validate',
                    'message' => (string) $errors,
                ]);

                continue;
            }

            $article = new FeedItemDto(
                $htmlDto->getId(),
                $htmlDto->getTitle(),
                $htmlDto->getUrl(),
                $htmlDto->getDatetime(),
            );

            $articles[] = $article;
        }

        return new FeedDto($articles);
    }
}
