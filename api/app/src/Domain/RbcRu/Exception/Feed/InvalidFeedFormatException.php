<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Exception\Feed;

class InvalidFeedFormatException extends \Exception
{

}