<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220211033559 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE rbc_article_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE rbc_article_image_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE rbc_article (id INT NOT NULL, feed_item_id INT NOT NULL, title VARCHAR(255) NOT NULL, lead TEXT NOT NULL, content TEXT NOT NULL, tags TEXT NOT NULL, external_id VARCHAR(255) NOT NULL, datetime_published TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, authors TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6E650380A87D462B ON rbc_article (feed_item_id)');
        $this->addSql('COMMENT ON COLUMN rbc_article.tags IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN rbc_article.authors IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE rbc_article_image (id INT NOT NULL, article_id INT NOT NULL, url TEXT NOT NULL, author VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_58A467D07294869C ON rbc_article_image (article_id)');
        $this->addSql('ALTER TABLE rbc_article ADD CONSTRAINT FK_6E650380A87D462B FOREIGN KEY (feed_item_id) REFERENCES rbc_feed_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rbc_article_image ADD CONSTRAINT FK_58A467D07294869C FOREIGN KEY (article_id) REFERENCES rbc_article (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rbc_article_image DROP CONSTRAINT FK_58A467D07294869C');
        $this->addSql('DROP SEQUENCE rbc_article_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE rbc_article_image_id_seq CASCADE');
        $this->addSql('DROP TABLE rbc_article');
        $this->addSql('DROP TABLE rbc_article_image');
    }
}
