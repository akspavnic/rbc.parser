<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220211034815 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE rbc_article_image_id_seq CASCADE');
        $this->addSql('DROP TABLE rbc_article_image');
        $this->addSql('ALTER TABLE rbc_article ADD images TEXT NOT NULL');
        $this->addSql('COMMENT ON COLUMN rbc_article.images IS \'(DC2Type:array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE rbc_article_image_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE rbc_article_image (id INT NOT NULL, article_id INT NOT NULL, url TEXT NOT NULL, author VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_58a467d07294869c ON rbc_article_image (article_id)');
        $this->addSql('ALTER TABLE rbc_article_image ADD CONSTRAINT fk_58a467d07294869c FOREIGN KEY (article_id) REFERENCES rbc_article (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rbc_article DROP images');
    }
}
