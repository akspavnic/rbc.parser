<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220210055740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE rbc_feed_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE rbc_feed_item (id INT NOT NULL, external_id VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, url TEXT NOT NULL, datetime_published TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E9198AC19F75D7B0 ON rbc_feed_item (external_id)');
        $this->addSql('CREATE INDEX IDX_E9198AC12E245AA8 ON rbc_feed_item (datetime_published)');
        $this->addSql('CREATE INDEX IDX_E9198AC18B8E8428 ON rbc_feed_item (created_at)');
        $this->addSql('COMMENT ON COLUMN rbc_feed_item.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN rbc_feed_item.updated_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE rbc_feed_item_id_seq CASCADE');
        $this->addSql('DROP TABLE rbc_feed_item');
    }
}
