<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Request\Article;

class ArticleDetailPageRequest implements \App\Component\Parser\HttpClient\HttpRequestInterface
{
    private const METHOD = 'GET';

    public function __construct(private string $url)
    {
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getMethod(): string
    {
        return self::METHOD;
    }

    public function getOptions(): array
    {
        return [];
    }
}