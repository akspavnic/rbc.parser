<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Request\Feed;

use App\Component\Parser\HttpClient\HttpRequestInterface;

class NewsFeedIndexPageRequest implements HttpRequestInterface
{
    private const URL = 'https://www.rbc.ru';
    private const METHOD = 'GET';

    public function getUrl(): string
    {
        return self::URL;
    }

    public function getMethod(): string
    {
        return self::METHOD;
    }

    public function getOptions(): array
    {
        return [];
    }
}