<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Request\Feed;

use App\Component\Parser\HttpClient\HttpRequestInterface;

class NewsFeedJsonEndpointRequest implements HttpRequestInterface
{
    private const URL = 'https://www.rbc.ru/v10/ajax/get-news-feed/project/rbcnews.uploaded/modifDate/#TS#/?_=';
    private const METHOD = 'GET';

    public function __construct(private ?int $timestamp = null)
    {
        $this->timestamp = $this->timestamp ?? (new \DateTime())->getTimestamp();
    }

    public function getUrl(): string
    {
        return str_replace('#TS#', (string)$this->timestamp, self::URL);
    }

    public function getMethod(): string
    {
        return self::METHOD;
    }

    public function getOptions(): array
    {
        return [];
    }
}