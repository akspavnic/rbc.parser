<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Domain\RbcRu\Repository\FeedItemRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FeedItemRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[Orm\Table(name: 'rbc_feed_item')]
#[ORM\Index(fields: ['externalId'])]
#[ORM\Index(fields: ['datetimePublished'])]
#[ORM\Index(fields: ['createdAt'])]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'path' => '/rbc/feed'
        ],
    ],
    itemOperations: [
        'get' => [
            'path' => '/rbc/feed/{id}',
            'requirements' => ['id' => '\d+'],
        ]
    ],
    normalizationContext: [
        'groups' => [
            'FeedItem'
        ],
        'skip_null_values' => false,
    ],
    order: [
        'id' => 'DESC'
    ],
)]
class FeedItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["FeedItem", 'Article'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["FeedItem"])]
    private $externalId;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["FeedItem"])]
    private $title;

    #[ORM\Column(type: 'text')]
    #[Groups(["FeedItem"])]
    private $url;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(["FeedItem"])]
    private $datetimePublished;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(["FeedItem"])]
    private $createdAt;

    #[ORM\OneToOne(
        mappedBy: 'feedItem',
        targetEntity: Article::class,
        cascade: ["persist", "remove"]
    )]
    #[Groups(['FeedItem'])]
    private $article;

    #[Groups(["FeedItem"])]
    private ?bool $hasArticle;

    public function getHasArticle(): ?bool
    {
        return null !== $this->getArticle();
    }

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getDatetimePublished(): ?\DateTimeInterface
    {
        return $this->datetimePublished;
    }

    public function setDatetimePublished(?\DateTimeInterface $datetimePublished): self
    {
        $this->datetimePublished = $datetimePublished;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(Article $article): self
    {
        // set the owning side of the relation if necessary
        if ($article->getFeedItem() !== $this) {
            $article->setFeedItem($this);
        }

        $this->article = $article;

        return $this;
    }
}
