<?php

declare(strict_types=1);

namespace App\Domain\RbcRu\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Domain\RbcRu\Repository\ArticleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
#[Orm\Table(name: 'rbc_article')]
#[ORM\Index(fields: ['externalId'])]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'path' => '/rbc/article',
            'normalization_context' => [
                'groups' => [
                    'Article',
                ],
            ],
        ],
    ],
    itemOperations: [
        'get' => [
            'path' => '/rbc/article/{id}',
            'requirements' => ['id' => '\d+'],
            'normalization_context' => [
                'groups' => [
                    'Article',
                    'ArticleDetails',
                ],
            ],
        ]
    ],
    normalizationContext: [
        'groups' => [
            'Article'
        ],
        'skip_null_values' => false,
    ],
)]
class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['Article', 'FeedItem'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['Article', 'FeedItem'])]
    private $title;

    #[ORM\Column(type: 'text')]
    #[Groups(['Article', 'FeedItem'])]
    private $lead;

    #[ORM\Column(type: 'text')]
    #[Groups(['ArticleDetails'])]
    private $content;

    #[ORM\Column(type: 'array')]
    #[Groups(['Article'])]
    private $tags = [];

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['Article'])]
    private $externalId;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['Article'])]
    private $datetimePublished;

    #[ORM\OneToOne(
        inversedBy: 'article',
        targetEntity: FeedItem::class,
        cascade: ["persist", "remove"]
    )]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['Article'])]
    private $feedItem;

    #[ORM\Column(type: 'array')]
    #[Groups(['Article'])]
    private $authors = [];

    #[ORM\Column(type: 'array')]
    #[Groups(['Article'])]
    private $images = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLead(): string
    {
        return $this->lead;
    }

    public function setLead(string $lead): self
    {
        $this->lead = $lead;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getDatetimePublished(): ?\DateTimeInterface
    {
        return $this->datetimePublished;
    }

    public function setDatetimePublished(?\DateTimeInterface $datetimePublished): self
    {
        $this->datetimePublished = $datetimePublished;

        return $this;
    }

    public function getFeedItem(): FeedItem
    {
        return $this->feedItem;
    }

    public function setFeedItem(FeedItem $feedItem): self
    {
        $this->feedItem = $feedItem;

        return $this;
    }

    public function getAuthors(): array
    {
        return $this->authors;
    }

    public function setAuthors(array $authors): self
    {
        $this->authors = $authors;

        return $this;
    }

    public function getImages(): array
    {
        return $this->images;
    }

    public function setImages(array $images): self
    {
        $this->images = $images;

        return $this;
    }
}
