## Build images and run containers

```
cd api
docker-compose up -d --build
```

## Install dependencies

```
docker-compose exec app cp .env.local.dist .env
docker-compose exec app composer install -o
```

## Run migrations

```
docker-compose exec app bin/console doctrine:migrations:migrate -n
```

## App cmds

Just save current feed

```
docker-compose exec app bin/console app:rbc-ru:feed:save
```

Save and sent task in queue for parsing articles detail page

```
docker-compose exec app bin/console app:rbc-ru:feed:save --save-articles=1 -vvv
```

Run consumer

```
docker-compose exec app bin/console messenger:consume rbc_articles -vvv
```

Stop consumer

```
ctrl+c
```

**!!Важно!!** Парсинг некоторых детальных страниц (статей) может падать, так как у rbc.ru несколько шаблонов страниц и под все парсинг \
не настраивал.

## Run internal http server

```
docker-compose exec app symfony serve
```

Stop internal http server

```
ctrl+c
```

## API

### docs & try it out

http://localhost:8022/api

## Toto

- Add tests (Codeception)
- Add supervisord for autostart consumers and parsers
- Add health check metrics (Prometheus) and visualize it in Grafana
- Optimize Dockerfile for production
